/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.normaliser.tika;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.tika.exception.TikaException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;
import org.springframework.beans.BeansException;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class CorpusTest {


	TikaExtractorService extractorService;


	@Before
	public void setUp() throws BeansException, TikaException, IOException {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml")) {
			final TikaConfiguration conf = fsxac.getBean("theConf", TikaConfiguration.class);
			conf.setGenerateHtml(true);
			this.extractorService = new TikaExtractorService(conf);
		}
	}


	@Test
	public void testNormalise() throws Exception {

		/*
		 * File srcFolder = new File("src/test/resources/arabicPDF");
		 * File destFolder = new File("target/corpus/arabicPDF");
		 */

		/*-----------------------------------------------------
		 * test only Html documents ( CustomBoilerpipeHtmlParser)
		 *------------------------------------------------------*/
		// File srcFolder = new File("src/test/resources/htmlDocs/");
		// File destFolder = new File("target/corpus/htmlDocs_Outputs/");
		//

		final File srcFolder = new File("src/test/resources/testDocs");
		final File destFolder = new File("target/corpus/testDocs_Outputs");


		if (!destFolder.exists()) {
			destFolder.mkdirs();
		}

		this.normalise(srcFolder, destFolder);
	}


	private void normalise(final File folderToCrawl, final File destFolder) throws IOException, AccessDeniedException, ContentNotAvailableException, InsufficientResourcesException,
	InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException, WebLabCheckedException, URISyntaxException {
		for (final File fileToNormalise : folderToCrawl.listFiles()) {

			if (!fileToNormalise.isHidden()) {
				if (fileToNormalise.isDirectory()) {
					final File subDestFolder = new File(destFolder, fileToNormalise.getName());
					if (!subDestFolder.exists()) {
						subDestFolder.mkdir();
					}

					this.normalise(fileToNormalise, subDestFolder);
				} else {
					final long nanoTime = System.nanoTime();
					final String docUri = "weblab://" + nanoTime;

					final ProcessArgs pargs = new ProcessArgs();
					final Document doc = new Document();
					doc.setUri(docUri);
					pargs.setResource(doc);
					final ContentManager contentManager = ContentManager.getInstance();
					final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
					try (FileInputStream stream = new FileInputStream(fileToNormalise)) {
						wpa.writeNativeContent(contentManager.create(stream));
					}
					final ProcessReturn pReturn = this.extractorService.process(pargs);
					pReturn.getResource();

					final File destHtmlFile = new File(destFolder, fileToNormalise.getName() + ".html");
					FileUtils.copyInputStreamToFile(contentManager.read(wpa.readNormalisedContent().firstTypedValue()), destHtmlFile);
					final File destFileXml = new File(destFolder, fileToNormalise.getName() + ".xml");
					new WebLabMarshaller().marshalResource(pReturn.getResource(), destFileXml);
					Assert.assertNotNull("WEBLAB-1424", contentManager.readLocalExistingFile(wpa.readNativeContent().firstTypedValue(), null, null));
				}
			}
		}
	}

}
