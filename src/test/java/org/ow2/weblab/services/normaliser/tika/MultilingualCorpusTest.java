/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.normaliser.tika;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import org.apache.tika.exception.TikaException;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.AccessDeniedException;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ServiceNotConfiguredException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.UnsupportedRequestException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;
import org.springframework.beans.BeansException;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class MultilingualCorpusTest {


	TikaExtractorService extractorService;


	@Before
	public void setUp() throws BeansException, TikaException, IOException {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml")) {
			this.extractorService = new TikaExtractorService(fsxac.getBean("theConf", TikaConfiguration.class));
		}
	}


	@Test
	public void testNormalise() throws Exception {

		/*-----------------------------------------------------
		 * test multilingual documents
		 *------------------------------------------------------*/
		final File srcFolder = new File("src/test/resources/multiLingualDocs/");
		final File destFolder = new File("target/corpus/multiLingualDocs_XmlOutputs/");

		if (!destFolder.exists()) {
			destFolder.mkdirs();
		}

		this.normalise(srcFolder, destFolder);
	}


	private void normalise(final File folderToCrawl, final File destFolder) throws IOException, AccessDeniedException, ContentNotAvailableException, InsufficientResourcesException,
			InvalidParameterException, ServiceNotConfiguredException, UnexpectedException, UnsupportedRequestException, WebLabCheckedException, URISyntaxException, InterruptedException {

		for (final File fileToNormalise : folderToCrawl.listFiles()) {
			if (fileToNormalise.isHidden() || fileToNormalise.getName().startsWith(".")) {
				//
			} else if (fileToNormalise.isDirectory()) {
				final File subDestFolder = new File(destFolder, fileToNormalise.getName());
				if (!subDestFolder.exists()) {
					subDestFolder.mkdir();
				}

				this.normalise(fileToNormalise, subDestFolder);
			} else {

				final ProcessArgs pargs = new ProcessArgs();
				final Document doc = WebLabResourceFactory.createResource(this.getClass().getName(), fileToNormalise.getName(), Document.class);
				pargs.setResource(doc);
				try (FileInputStream stream = new FileInputStream(fileToNormalise)) {
					new WProcessingAnnotator(doc).writeNativeContent(ContentManager.getInstance().create(stream));
				}

				final ProcessReturn pReturn = this.extractorService.process(pargs);
				final Resource resReturn = pReturn.getResource();

				// Extract Text Units from Resource
				List<Text> texts_l = new LinkedList<>();
				if (resReturn instanceof Document) {
					texts_l = ResourceUtil.getSelectedSubResources(resReturn, Text.class);
				} else if (resReturn instanceof Text) {
					texts_l.add((Text) resReturn);
				}

				final File destFile = new File(destFolder, fileToNormalise.getName() + ".xml");
				new WebLabMarshaller().marshalResource(resReturn, destFile);
			}
		}
	}

}
