/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.normaliser.tika;

import java.io.File;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;
import org.springframework.beans.BeansException;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class TikaExtractorParamsTest {


	TikaExtractorService ees;


	ProcessArgs pa;


	public TikaExtractorParamsTest() throws BeansException, TikaException, IOException {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml")) {
			this.ees = new TikaExtractorService(fsxac.getBean("theConf", TikaConfiguration.class));
		}
		this.pa = new ProcessArgs();
	}


	@Test
	public void testNoContentFile() throws Exception {
		final Document doc = new WebLabMarshaller().unmarshal(new File("src/test/resources/xmlInputs/inputWithNoContent.xml"), Document.class);
		this.pa.setResource(doc);
		try {
			this.ees.process(this.pa);
			Assert.fail("An Exception shall have been thrown.");
		} catch (final Exception e) {
			Assert.assertEquals(ContentNotAvailableException.class.getCanonicalName(), e.getClass().getCanonicalName());
		}
	}


	@Test
	public void testNullParams() throws Exception {
		try {
			this.ees.process(this.pa);
			Assert.fail("An Exception shall have been thrown.");
		} catch (final InvalidParameterException ipe) {
			Assert.assertNotNull(ipe);
		}
		this.pa = null;
		try {
			this.ees.process(this.pa);
			Assert.fail("An Exception shall have been thrown.");
		} catch (final InvalidParameterException ipe) {
			Assert.assertNotNull(ipe);
		}
	}


	@Test
	public void testBadResInParam() {
		this.pa.setResource(WebLabResourceFactory.createResource("sdssdqs", "dsfdfs", Text.class));
		try {
			this.ees.process(this.pa);
			Assert.fail("An Exception shall have been thrown.");
		} catch (final Exception e) {
			Assert.assertEquals(InvalidParameterException.class.getCanonicalName(), e.getClass().getCanonicalName());
		}
	}


	@Test
	public void testBadAnnotInParam() {
		this.pa.setResource(WebLabResourceFactory.createResource("sdssdqs", "dsfdfs", Document.class));
		try {
			this.ees.process(this.pa);
			Assert.fail("An Exception shall have been thrown.");
		} catch (final Exception e) {
			Assert.assertEquals(ContentNotAvailableException.class.getCanonicalName(), e.getClass().getCanonicalName());

		}
	}

}
