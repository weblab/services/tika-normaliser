/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.normaliser.tika;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.service.normaliser.tika.Constants;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;


/**
 * Just a simple test to check that every message key has a value in the properties file and vice-versa.
 *
 * @author WebLab IPCC Team
 */
public class MessagesTest {


	@Test
	public void testMessages() throws Exception {

		final Set<String> keys = new HashSet<>();
		for (final Field field : Constants.class.getDeclaredFields()) {
			if (field.getName().startsWith("KEY") && field.getType().equals(String.class)) {
				final String value = (String) field.get(null); // null since fields are static
				final String message = Messages.getString(ResourceBundle.getBundle(TikaExtractorService.class.getCanonicalName()), value);
				Assert.assertFalse("Key " + value + " not found.", message.startsWith("!") && message.endsWith("!"));
				keys.add(value);
			}
		}
		final Properties prop = new Properties();
		try (FileInputStream stream = new FileInputStream("src/main/resources/" + TikaExtractorService.class.getCanonicalName().replace('.', '/') + ".properties")) {
			prop.load(stream);
		}
		Assert.assertEquals("The keys loaded through instrospection are not equals to the keys loaded through property mecanism.", keys, prop.keySet());
	}

}
