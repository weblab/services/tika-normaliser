/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.normaliser.tika;

import java.io.File;
import java.io.FileInputStream;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class MetadataOnlyTest {


	@Test
	public void testExtractHtmlMetadata() throws Exception {
		final TikaExtractorService metadataExtractor;
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/applicationContext.xml")) {
			metadataExtractor = fsxac.getBean("metadataExtractor", TikaExtractorService.class);
		}

		final Document doc = WebLabResourceFactory.createResource(this.getClass().getName(), "html", Document.class);
		try (FileInputStream stream = new FileInputStream(new File("src/test/resources/testDocs/input.html"))) {
			new WProcessingAnnotator(doc).writeNativeContent(ContentManager.getInstance().create(stream));
		}
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(doc);
		metadataExtractor.process(pa);

		Assert.assertTrue(doc.getMediaUnit().isEmpty());
		final DublinCoreAnnotator dublinCoreAnnotator = new DublinCoreAnnotator(doc);
		Assert.assertEquals("text/html", dublinCoreAnnotator.readFormat().firstTypedValue());
		Assert.assertEquals("Title : Tilte with UTF-8 chars öäå", dublinCoreAnnotator.readTitle().firstTypedValue());
	}

}
