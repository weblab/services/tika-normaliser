/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.services.normaliser.tika;

import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.springframework.beans.BeansException;


public class TikaOutlookExtractorTest extends AbstractTika {


	public TikaOutlookExtractorTest() throws BeansException, TikaException, IOException {
		super();
	}


	@Test
	public void testExtractTextWork() throws Exception {
		final Document doc = WebLabResourceFactory.createResource(this.getClass().getName(), "msg", Document.class);
		this.extract("src/test/resources/testDocs/input.msg", doc);
	}

}
