/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.normaliser.tika;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;

import org.apache.tika.exception.TikaException;
import org.junit.Assert;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;
import org.springframework.beans.BeansException;

public abstract class AbstractTika {


	TikaExtractorService ees;


	ProcessArgs pa;


	public AbstractTika() throws BeansException, TikaException, IOException {
		final TikaConfiguration conf = new TikaConfiguration();
		// Generate random values
		conf.setAddMetadata(Math.random() > 0.5);
		conf.setGenerateHtml(Math.random() > 0.5);
		conf.setRemoveTempContent(Math.random() > 0.5);
		conf.setServiceUri(Math.random() > 0.5 ? null : "");
		this.ees = new TikaExtractorService(conf);
		this.pa = new ProcessArgs();
	}


	public Document extract(final String filepath, final Document doc) throws Exception {

		final URI nativeContent;
		try (FileInputStream stream = new FileInputStream(new File(filepath))) {
			nativeContent = ContentManager.getInstance().create(stream);
		}
		new WProcessingAnnotator(doc).writeNativeContent(nativeContent);
		this.pa.setResource(doc);
		this.ees.process(this.pa);

		final String content = ((Text) doc.getMediaUnit().get(0)).getContent();
		Assert.assertNotNull(content);
		Assert.assertNotNull("WEBLAB-1424", ContentManager.getInstance().readLocalExistingFile(nativeContent, null, null));
		return doc;
	}

}
