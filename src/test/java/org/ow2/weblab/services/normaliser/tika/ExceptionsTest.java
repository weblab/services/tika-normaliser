/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.normaliser.tika;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.DublinCore;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.service.normaliser.tika.Constants;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.ow2.weblab.service.normaliser.tika.TikaExtractorService;
import org.ow2.weblab.service.normaliser.tika.mapping.DatePropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.IntegerPropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.MessageFormatMD;
import org.ow2.weblab.service.normaliser.tika.mapping.URIPropertyMD;


/**
 * @author ymombrun
 */
public class ExceptionsTest {


	final static URI FAKE_URI = URI.create("weblab:toto");


	@Test(expected = IllegalArgumentException.class)
	public void testInitBadDateMD() {
		new DatePropertyMD(DublinCore.CONTRIBUTOR, FAKE_URI.toASCIIString(), null, false, false);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testInitBadIntegerMD() {
		new IntegerPropertyMD(DublinCore.CONTRIBUTOR, FAKE_URI.toASCIIString(), null, false, false);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testInitBadMessageFormatMD() {
		new MessageFormatMD(DublinCore.CONTRIBUTOR, FAKE_URI.toASCIIString(), null, false, false, null);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testInitBadUriMD() {
		new URIPropertyMD(DublinCore.CONTRIBUTOR, FAKE_URI.toASCIIString(), null, false, false);
	}

	@Test(expected = IOException.class)
	public void testBadTikaConfiguration() throws TikaException, IOException {
		TikaConfiguration conf = new TikaConfiguration();
		conf.setPathToXmlConfigurationFile("something that does not exist in classpath!?");
		new TikaExtractorService(conf);
	}


	@Test
	public void testConstructor() throws Exception {
		try {
			for (final Constructor<?> c : Constants.class.getDeclaredConstructors()) {
				c.setAccessible(true);
				c.newInstance();
				Assert.fail();
			}
			Assert.fail();
		} catch (final InvocationTargetException ite) {
			if (!(ite.getCause() instanceof UnsupportedOperationException)) {
				Assert.fail("Cause of failure was not an UnsupportedOperationException but a " + ite.getCause());
			}
		}
	}


}
