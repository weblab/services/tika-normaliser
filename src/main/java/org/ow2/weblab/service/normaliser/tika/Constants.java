/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika;

/**
 * This class contains public constants used in the Tika service.
 *
 * @author WebLab IPCC Team
 */
public final class Constants {


	private Constants() {
		throw new UnsupportedOperationException("This class only contains static methods; no need to instantiate it.");
	}

	public static final String SERVICE_NAME = "Tika Normaliser";
	
	
	public static final String KEY_ERROR_UNABLE_TO_LOAD_CONTENT_MANAGER = "tika.message.error.unableToLoadContentManager";


	public static final String KEY_ERROR_CONTENT_NOT_AVAILABLE_1 = "tika.message.error.contentNotAvailable.1";


	public static final String KEY_ERROR_PROCESSARGS_NULL = "tika.message.error.processArgsNull";


	public static final String KEY_ERROR_RESOURCE_NULL = "tika.message.error.resourceNull";


	public static final String KEY_ERROR_NOT_A_DOCUMENT_2 = "tika.message.error.notADocument.2";


	public static final String KEY_ERROR_IOE_ON_CONTENT_2 = "tika.message.error.ioeOnContent.2";


	public static final String KEY_ERROR_IOE_ON_CONTENT_SIMPLE = "tika.message.error.ioeOnContentSimple";


	public static final String KEY_ERROR_ERROR_ON_CONTENT_SIMPLE = "tika.message.error.errorOnContentSimple";


	public static final String KEY_ERROR_SAXE_ON_CONTENT_2 = "tika.message.error.saxeOnContent.2";


	public static final String KEY_ERROR_TIKA_EX_ON_CONTENT_2 = "tika.message.error.tikaExOnContent.2";


	public static final String KEY_ERROR_BAD_HANDLER_1 = "tika.message.error.badHandler.1";


	public static final String KEY_ERROR_CONTENT_FILE_NOT_FOUND_2 = "tika.message.error.contentFileNotFound.2";


	public static final String KEY_ERROR_CONTENT_FILE_NOT_READABLE_2 = "tika.message.error.contentFileNotReadable.2";




	public static final String KEY_INFO_END_OF_PROCESS_1 = "tika.message.info.endOfprocess.1";


	public static final String KEY_INFO_PROCESS_DOCUMENT_1 = "tika.message.info.processCalled.1";


	public static final String KEY_INFO_SERVICE_STARTED = "tika.message.info.started";


	public static final String KEY_WARN_MSG_NOT_FOUND_1 = "tika.message.warn.msgNotFound.1";


	public static final String KEY_WARN_NO_TEXT_FOUND_2 = "tika.message.warn.noTextFound.2";


	public static final String KEY_WARN_UNABLE_TO_DELETE_TEMP_2 = "tika.message.warn.unableToDeleteTemp.2";


	public static final String KEY_WARN_MORE_THAN_ONE_TYPE_2 = "tika.message.warn.moreThanOneType.2";


	public static final String KEY_WARN_NOT_A_COMPOSITE_PARSER_1 = "tika.message.warn.notACompositeParser.1";


	public static final String KEY_WARN_UNABLE_TO_CREATE_TEMP_FILE_1 = "tika.message.warn.unableToCreateTempFile.1";


	public static final String KEY_WARN_UNABLE_TO_CREATE_TRANSFORMER_1 = "tika.message.warn.unableToCreateTransformer.1";


	public static final String KEY_WARN_NO_OUTPUT_FILE_2 = "tika.message.warn.noOutputFile.2";


	public static final String KEY_WARN_EMPTY_OUTPUT_FILE_2 = "tika.message.warn.emptyOutputFile.2";


	public static final String KEY_WARN_ERROR_SAVING_NORMALISED_2 = "tika.message.warn.errorSavingNormalised.2";


	public static final String KEY_WARN_ERROR_COMMIT_2 = "tika.message.warn.errorCommit.2";


	public static final String KEY_WARN_NO_META_1 = "tika.message.warn.noMeta.1";


	public static final String KEY_WARN_UNMAPPED_PROPERTY_ERROR_4 = "tika.message.warn.unmappedPropertyError.4";


	public static final String KEY_DEBUG_DEFAULT_TIKA_CONFIG = "tika.message.debug.defaultTikaConf";


	public static final String KEY_ERROR_CANNOT_LOAD_TIKA_CONFIG_1 = "tika.message.error.cannotLoadTikaConfig.1";


	public static final String KEY_WARN_NO_PARSER_FOR_TYPE_1 = "tika.message.warn.noParserForType";


}
