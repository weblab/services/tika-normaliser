/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.handlers;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.service.normaliser.tika.Constants;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * This class is the simplest exemple of WebLab Handler Decorator.
 *
 * It creates a single Text unit with the concatenation of the HTML content in the Handler.
 *
 * @author WebLab IPCC Team
 */
public class SimpleTextContentHandler extends WebLabHandlerDecorator {


	private static final String BODY = "body";


	private final static List<String> NEWLINE_ELEMENTS = Arrays.asList("ol", "dt", "dl", "ul", "li", "br", "p", "div", "table", "tr");


	private final static List<String> TAB_ELEMENTS = Arrays.asList("td");


	private boolean isInBody = false;


	private Document document;


	private Text createdText;


	private final StringBuilder sb;


	private final Log logger;


	public SimpleTextContentHandler() {
		super();
		this.sb = new StringBuilder();
		this.logger = LogFactory.getLog(this.getClass());
		this.logger.trace("SimpleTextContentHandler initialised.");
	}


	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes atts) throws SAXException {
		super.startElement(uri, localName, name, atts);

		if (SimpleTextContentHandler.BODY.equalsIgnoreCase(name)) {
			this.isInBody = true;
		} else if (this.isInBody && SimpleTextContentHandler.NEWLINE_ELEMENTS.contains(name)) {
			this.sb.append("\n");
		}

	}


	@Override
	public void endElement(final String uri, final String localName, final String name) throws SAXException {
		super.endElement(uri, localName, name);

		if (SimpleTextContentHandler.BODY.equalsIgnoreCase(name)) {
			this.createdText.setContent(this.sb.toString().trim());
			this.isInBody = false;

			if (this.createdText.getContent().trim().isEmpty()) {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process the document. "+"The MediaUnit " + this.createdText.getUri() + " will be removed since it is empty.");
				this.document.getMediaUnit().remove(this.createdText);
			}
		} else if (this.isInBody && SimpleTextContentHandler.NEWLINE_ELEMENTS.contains(name)) {
			this.sb.append("\n");
		} else if (this.isInBody && SimpleTextContentHandler.TAB_ELEMENTS.contains(name)) {
			this.sb.append("\t");
		} else if (this.isInBody) {
			this.sb.append(" ");
		}

	}


	@Override
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		super.characters(ch, start, length);

		if (this.isInBody) {
			// Replace any duplicate space (or tab or newline) by a single space and trim the whole
			final String theContent = new String(Arrays.copyOfRange(ch, start, start + length)).replaceAll("\\s+", " ");

			if (!theContent.isEmpty()) {
				this.sb.append(theContent);
			}
		}

	}


	@Override
	public void setDocument(final Document document) {
		this.document = document;
		this.createdText = WebLabResourceFactory.createAndLinkMediaUnit(this.document, Text.class);
	}


	@Override
	public void setTikaConfiguration(final TikaConfiguration tikaConfiguration) {
		// Nothing to do.
	}

}
