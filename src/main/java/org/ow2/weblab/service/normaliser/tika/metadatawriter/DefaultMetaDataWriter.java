/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.metadatawriter;

import org.apache.tika.metadata.Geographic;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.IPTC;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import org.apache.tika.metadata.OfficeOpenXMLExtended;
import org.apache.tika.metadata.PagedText;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.TIFF;
import org.apache.tika.metadata.XMP;
import org.apache.tika.metadata.XMPDM;
import org.apache.tika.metadata.XMPRights;
import org.ow2.weblab.core.extended.ontologies.DCTerms;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.MediaOntology;
import org.ow2.weblab.core.extended.ontologies.RDFS;
import org.ow2.weblab.service.normaliser.tika.mapping.FormatWithCharsetPropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.DatePropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.FormatPropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.IntegerPropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.MessageFormatMD;
import org.ow2.weblab.service.normaliser.tika.mapping.RealPropertyMD;
import org.ow2.weblab.service.normaliser.tika.mapping.TextPropertyMD;

/**
 * Default implementation class of {@link MetadataWriter MetadataWriter}. It
 * contains a mapping for several standard properties from DublinCore annotation
 * sets.
 *
 * @author Frédéric Praca
 * @see MetadataWriter
 * @see org.apache.tika.metadata.Metadata
 * @see TextPropertyMD
 */
public class DefaultMetaDataWriter extends MetadataWriter {


	protected static final String WGS_LONG = "http://www.w3.org/2003/01/geo/wgs84_pos#long";


	protected static final String WGS_LAT = "http://www.w3.org/2003/01/geo/wgs84_pos#lat";


	protected static final String PREFIX_WGS = "wgs84";


	protected static final String PREFIX_RDFS = "rdfs";


	protected static final String PREFIX_DC = "dc";


	protected static final Property CONTENT_TYPE = Property.internalText(HttpHeaders.CONTENT_TYPE);


	private final boolean addLanguage;


	/**
	 * Default constructor preventing from adding language extracted metadata
	 */
	public DefaultMetaDataWriter() {
		this(false);
	}


	/**
	 * @param addLanguage
	 *            Whether or not to add language
	 */
	public DefaultMetaDataWriter(final boolean addLanguage) {
		this.addLanguage = addLanguage;
	}


	@Override
	public void init() {
		/*
		 * DublinCore Metadata mapping
		 *
		 * The easiest one. Every property is mapped either to DublinCore or DCTerms
		 */
		this.addMapping(org.apache.tika.metadata.DublinCore.CONTRIBUTOR,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.CONTRIBUTOR, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.COVERAGE,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.COVERAGE, DublinCore.COVERAGE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.CREATED, new DatePropertyMD(org.apache.tika.metadata.DublinCore.CREATED, DCTerms.CREATED, DCTerms.PREFERRED_PREFIX, false, true));
		this.addMapping(org.apache.tika.metadata.DublinCore.CREATOR,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.CREATOR, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.DATE,
				new DatePropertyMD(org.apache.tika.metadata.DublinCore.DATE, DublinCore.DATE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.DESCRIPTION,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.DESCRIPTION, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.FORMAT, new FormatPropertyMD(org.apache.tika.metadata.DublinCore.FORMAT));
		this.addMapping(org.apache.tika.metadata.DublinCore.FORMAT, new FormatWithCharsetPropertyMD(org.apache.tika.metadata.DublinCore.FORMAT));
		this.addMapping(org.apache.tika.metadata.DublinCore.IDENTIFIER,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.IDENTIFIER, DublinCore.IDENTIFIER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		if (this.addLanguage) {
			this.addMapping(org.apache.tika.metadata.DublinCore.LANGUAGE,
					new TextPropertyMD(org.apache.tika.metadata.DublinCore.LANGUAGE, DublinCore.LANGUAGE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, false, true));
		}
		this.addMapping(org.apache.tika.metadata.DublinCore.MODIFIED, new DatePropertyMD(org.apache.tika.metadata.DublinCore.MODIFIED, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.PUBLISHER,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.PUBLISHER, DublinCore.PUBLISHER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.RELATION,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.RELATION, DublinCore.RELATION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.RIGHTS,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.RIGHTS, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.SOURCE,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.SOURCE, DublinCore.SOURCE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, false, true));
		this.addMapping(org.apache.tika.metadata.DublinCore.SUBJECT,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.SUBJECT, DublinCore.SUBJECT_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(org.apache.tika.metadata.DublinCore.TITLE,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.TITLE, DublinCore.TITLE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, false, true));
		this.addMapping(org.apache.tika.metadata.DublinCore.TYPE,
				new TextPropertyMD(org.apache.tika.metadata.DublinCore.TYPE, DublinCore.TYPE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));


		/*
		 * Office Metadata mapping
		 *
		 * Only the print date cannot be mapped.
		 * Also a lot of properties are mapped using the dct:extent + unit mapping
		 */
		this.addMapping(Office.AUTHOR, new TextPropertyMD(Office.AUTHOR, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(Office.CHARACTER_COUNT,
				new MessageFormatMD(Office.CHARACTER_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#character|1#character|1<characters}"));
		this.addMapping(Office.CHARACTER_COUNT_WITH_SPACES, new MessageFormatMD(Office.CHARACTER_COUNT_WITH_SPACES, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false,
				"{0,number,integer} {0,choice,0#character|1#character|1<characters} (with spaces)"));
		this.addMapping(Office.CREATION_DATE, new DatePropertyMD(Office.CREATION_DATE, DCTerms.CREATED, DCTerms.PREFERRED_PREFIX, false, true));
		this.addMapping(Office.IMAGE_COUNT, new MessageFormatMD(Office.IMAGE_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#image|1#image|1<images}"));
		this.addMapping(Office.INITIAL_AUTHOR, new TextPropertyMD(Office.INITIAL_AUTHOR, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(Office.KEYWORDS, new TextPropertyMD(Office.KEYWORDS, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(Office.LAST_AUTHOR, new TextPropertyMD(Office.LAST_AUTHOR, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(Office.LINE_COUNT, new MessageFormatMD(Office.LINE_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#line|1#line|1<lines}"));
		this.addMapping(Office.OBJECT_COUNT,
				new MessageFormatMD(Office.OBJECT_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#object|1#object|1<objects}"));
		this.addMapping(Office.PAGE_COUNT, new MessageFormatMD(Office.PAGE_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#page|1#page|1<pages}"));
		this.addMapping(Office.PARAGRAPH_COUNT,
				new MessageFormatMD(Office.PARAGRAPH_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#paragraph|1#paragraph|1<paragraphs}"));
		// this.addMapping(Office.PRINT_DATE,
		this.addMapping(Office.SAVE_DATE, new DatePropertyMD(Office.SAVE_DATE, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(Office.SLIDE_COUNT, new MessageFormatMD(Office.SLIDE_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#slide|1#slide|1<slides}"));
		this.addMapping(Office.TABLE_COUNT, new MessageFormatMD(Office.TABLE_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#table|1#table|1<tables}"));
		this.addMapping(Office.WORD_COUNT, new MessageFormatMD(Office.WORD_COUNT, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#word|1#word|1<words}"));


		/*
		 * OfficeOpenXMLCore Metadata mapping
		 *
		 * No holder on side for version, revision and print date
		 */
		this.addMapping(OfficeOpenXMLCore.CATEGORY, new TextPropertyMD(OfficeOpenXMLCore.CATEGORY, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(OfficeOpenXMLCore.CONTENT_STATUS, new TextPropertyMD(OfficeOpenXMLCore.CONTENT_STATUS, DCTerms.TYPE, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(OfficeOpenXMLCore.LAST_MODIFIED_BY, new TextPropertyMD(OfficeOpenXMLCore.LAST_MODIFIED_BY, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(OfficeOpenXMLCore.LAST_PRINTED
		// this.addMapping(OfficeOpenXMLCore.REVISION
		this.addMapping(OfficeOpenXMLCore.SUBJECT, new TextPropertyMD(OfficeOpenXMLCore.SUBJECT, DublinCore.SUBJECT_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(OfficeOpenXMLCore.VERSION


		/*
		 * OfficeOpenXMLCore Metadata mapping
		 *
		 * There is no javadoc comment on the Tika class, thus these properties are hard to map.
		 */
		// this.addMapping(OfficeOpenXMLExtended.APP_VERSION
		this.addMapping(OfficeOpenXMLExtended.APPLICATION, new TextPropertyMD(OfficeOpenXMLExtended.APPLICATION, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(OfficeOpenXMLExtended.COMMENTS, new TextPropertyMD(OfficeOpenXMLExtended.COMMENTS, RDFS.COMMENT, DefaultMetaDataWriter.PREFIX_RDFS, true, false));
		this.addMapping(OfficeOpenXMLExtended.COMPANY, new TextPropertyMD(OfficeOpenXMLExtended.COMPANY, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(OfficeOpenXMLExtended.DOC_SECURITY
		// this.addMapping(OfficeOpenXMLExtended.HIDDEN_SLIDES
		this.addMapping(OfficeOpenXMLExtended.MANAGER, new TextPropertyMD(OfficeOpenXMLExtended.MANAGER, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(OfficeOpenXMLExtended.NOTES
		// this.addMapping(OfficeOpenXMLExtended.PRESENTATION_FORMAT
		// this.addMapping(OfficeOpenXMLExtended.TEMPLATE
		// this.addMapping(OfficeOpenXMLExtended.TOTAL_TIME



		/*
		 * HttpHeaders Metadata Mapping.
		 *
		 * Other keys than last modified are String and not Property, we skip them but the content-type.
		 * Content-type seems to be used by many parser to define the format. But since this is not a Property but still a string, we define ourselves the property.
		 */
		this.addMapping(HttpHeaders.LAST_MODIFIED, new DatePropertyMD(HttpHeaders.LAST_MODIFIED, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(DefaultMetaDataWriter.CONTENT_TYPE, new FormatPropertyMD(DefaultMetaDataWriter.CONTENT_TYPE));
		this.addMapping(DefaultMetaDataWriter.CONTENT_TYPE, new FormatWithCharsetPropertyMD(DefaultMetaDataWriter.CONTENT_TYPE));

		/*
		 * XMP Metadata mapping
		 *
		 * Main scheme of the XMP metadata mapping. Rating is mapped to the media onto. We decided that when a property exists in mediaontology, dcterms and dc we always prefer the dc, then the
		 * dcterms.
		 */
		this.addMapping(XMP.CREATE_DATE, new TextPropertyMD(XMP.CREATE_DATE, DCTerms.CREATED, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMP.CREATOR_TOOL, new TextPropertyMD(XMP.CREATOR_TOOL, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(XMP.IDENTIFIER, new TextPropertyMD(XMP.IDENTIFIER, DublinCore.IDENTIFIER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(XMP.LABEL, new TextPropertyMD(XMP.LABEL, DCTerms.ALTERNATIVE, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMP.METADATA_DATE, new DatePropertyMD(XMP.METADATA_DATE, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMP.MODIFY_DATE, new TextPropertyMD(XMP.MODIFY_DATE, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMP.RATING, new TextPropertyMD(XMP.RATING, MediaOntology.RATING, MediaOntology.PREFERRED_PREFIX, true, false));


		/*
		 * XMPRights Metadata mapping
		 */
		this.addMapping(XMPRights.CERTIFICATE, new TextPropertyMD(XMPRights.CERTIFICATE, DCTerms.RIGHTS_STATEMENTS, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPRights.MARKED, ???, true, false);
		this.addMapping(XMPRights.OWNER, new TextPropertyMD(XMPRights.OWNER, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMPRights.USAGE_TERMS, new TextPropertyMD(XMPRights.USAGE_TERMS, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(XMPRights.WEB_STATEMENT, new TextPropertyMD(XMPRights.WEB_STATEMENT, DCTerms.RIGHTS_STATEMENTS, DCTerms.PREFERRED_PREFIX, true, false));


		/*
		 * XMPDM Metadata Mapping
		 *
		 * A lot cannot be mapped. They are often real based fields but Tika does not provied a getReal() method on metadata.
		 * Otherwise some could have been added to dct:extent or to media ontlogy specific but after a conversion...
		 */
		// this.addMapping(XMPDM.ABS_PEAK_AUDIO_FILE_PATH
		// this.addMapping(XMPDM.ALBUM
		this.addMapping(XMPDM.ALBUM_ARTIST, new TextPropertyMD(XMPDM.ALBUM_ARTIST, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(XMPDM.ALT_TAPE_NAME, new TextPropertyMD(XMPDM.ALT_TAPE_NAME, DCTerms.ALTERNATIVE, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMPDM.ARTIST, new TextPropertyMD(XMPDM.ARTIST, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(XMPDM.AUDIO_CHANNEL_TYPE
		// this.addMapping(XMPDM.AUDIO_COMPRESSOR
		this.addMapping(XMPDM.AUDIO_MOD_DATE, new DatePropertyMD(XMPDM.AUDIO_MOD_DATE, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPDM.AUDIO_SAMPLE_RATE
		// this.addMapping(XMPDM.AUDIO_SAMPLE_TYPE
		// this.addMapping(XMPDM.COMPILATION
		this.addMapping(XMPDM.COMPOSER, new TextPropertyMD(XMPDM.COMPOSER, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(XMPDM.COPYRIGHT, new TextPropertyMD(XMPDM.COPYRIGHT, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(XMPDM.DISC_NUMBER
		this.addMapping(XMPDM.DURATION, new TextPropertyMD(XMPDM.DURATION, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMPDM.ENGINEER, new TextPropertyMD(XMPDM.ENGINEER, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(XMPDM.FILE_DATA_RATE
		this.addMapping(XMPDM.GENRE, new TextPropertyMD(XMPDM.GENRE, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(XMPDM.INSTRUMENT
		this.addMapping(XMPDM.KEY, new TextPropertyMD(XMPDM.KEY, DublinCore.IDENTIFIER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(XMPDM.LOG_COMMENT, new TextPropertyMD(XMPDM.LOG_COMMENT, RDFS.COMMENT, DefaultMetaDataWriter.PREFIX_RDFS, true, false));
		// this.addMapping(XMPDM.LOOP
		this.addMapping(XMPDM.METADATA_MOD_DATE, new TextPropertyMD(XMPDM.METADATA_MOD_DATE, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPDM.NUMBER_OF_BEATS
		// this.addMapping(XMPDM.PULL_DOWN
		// this.addMapping(XMPDM.RELATIVE_PEAK_AUDIO_FILE_PATH
		this.addMapping(XMPDM.RELEASE_DATE, new TextPropertyMD(XMPDM.RELEASE_DATE, DCTerms.ISSUED, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPDM.SCALE_TYPE
		this.addMapping(XMPDM.SCENE, new TextPropertyMD(XMPDM.SCENE, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(XMPDM.SHOT_DATE
		this.addMapping(XMPDM.SHOT_LOCATION, new TextPropertyMD(XMPDM.SHOT_LOCATION, DCTerms.SPATIAL, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(XMPDM.SHOT_NAME, new TextPropertyMD(XMPDM.SHOT_NAME, DCTerms.ALTERNATIVE, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPDM.SPEAKER_PLACEMENT
		// this.addMapping(XMPDM.STRETCH_MODE
		this.addMapping(XMPDM.TAPE_NAME, new TextPropertyMD(XMPDM.TAPE_NAME, DCTerms.ALTERNATIVE, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPDM.TEMPO
		// this.addMapping(XMPDM.TIME_SIGNATURE
		// this.addMapping(XMPDM.TRACK_NUMBER
		// this.addMapping(XMPDM.VIDEO_ALPHA_MODE
		// this.addMapping(XMPDM.VIDEO_ALPHA_UNITY_IS_TRANSPARENT
		// this.addMapping(XMPDM.VIDEO_COLOR_SPACE
		// this.addMapping(XMPDM.VIDEO_COMPRESSOR
		// this.addMapping(XMPDM.VIDEO_FIELD_ORDER
		// this.addMapping(XMPDM.VIDEO_FRAME_RATE
		this.addMapping(XMPDM.VIDEO_MOD_DATE, new DatePropertyMD(XMPDM.VIDEO_MOD_DATE, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(XMPDM.VIDEO_PIXEL_ASPECT_RATIO
		// this.addMapping(XMPDM.VIDEO_PIXEL_DEPTH


		/*
		 * XMP PagedText Metadata mapping
		 */
		this.addMapping(PagedText.N_PAGES, new MessageFormatMD(PagedText.N_PAGES, DCTerms.EXTENT, DCTerms.PREFERRED_PREFIX, true, false, "{0,number,integer} {0,choice,0#page|1#page|1<pages}"));


		/*
		 * TIFF Metadata mapping.
		 *
		 * Lots of other properties are available but not yet mapped to "our" ontologies.
		 */
		// this.addMapping(TIFF.BITS_PER_SAMPLE
		// this.addMapping(TIFF.EQUIPMENT_MAKE
		// this.addMapping(TIFF.EQUIPMENT_MODEL
		// this.addMapping(TIFF.EXPOSURE_TIME
		// this.addMapping(TIFF.F_NUMBER
		// this.addMapping(TIFF.FLASH_FIRED
		// this.addMapping(TIFF.FOCAL_LENGTH
		this.addMapping(TIFF.IMAGE_LENGTH, new IntegerPropertyMD(TIFF.IMAGE_LENGTH, MediaOntology.FRAME_HEIGHT, MediaOntology.PREFERRED_PREFIX, false, true));
		this.addMapping(TIFF.IMAGE_WIDTH, new IntegerPropertyMD(TIFF.IMAGE_WIDTH, MediaOntology.FRAME_WIDTH, MediaOntology.PREFERRED_PREFIX, false, true));
		// this.addMapping(TIFF.ISO_SPEED_RATINGS
		// this.addMapping(TIFF.ORIENTATION
		this.addMapping(TIFF.ORIGINAL_DATE, new DatePropertyMD(TIFF.ORIGINAL_DATE, DCTerms.CREATED, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(TIFF.RESOLUTION_HORIZONTAL
		// this.addMapping(TIFF.RESOLUTION_UNIT
		// this.addMapping(TIFF.RESOLUTION_VERTICAL
		// this.addMapping(TIFF.SAMPLES_PER_PIXE
		this.addMapping(TIFF.SOFTWARE, new TextPropertyMD(TIFF.SOFTWARE, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));


		/*
		 * IPTC Metadata mapping
		 *
		 * Lots of other properties are available but not yet mapped to "our" ontologies.
		 */
		// this.addMapping(IPTC.ADDITIONAL_MODEL_INFO
		this.addMapping(IPTC.ARTWORK_OR_OBJECT, new TextPropertyMD(IPTC.ARTWORK_OR_OBJECT, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.ARTWORK_OR_OBJECT_DETAIL_COPYRIGHT_NOTICE,
				new TextPropertyMD(IPTC.ARTWORK_OR_OBJECT_DETAIL_COPYRIGHT_NOTICE, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.ARTWORK_OR_OBJECT_DETAIL_CREATOR,
				new TextPropertyMD(IPTC.ARTWORK_OR_OBJECT_DETAIL_CREATOR, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.ARTWORK_OR_OBJECT_DETAIL_DATE_CREATED, new TextPropertyMD(IPTC.ARTWORK_OR_OBJECT_DETAIL_DATE_CREATED, DCTerms.CREATED, DCTerms.PREFERRED_PREFIX, false, true));
		this.addMapping(IPTC.ARTWORK_OR_OBJECT_DETAIL_SOURCE,
				new TextPropertyMD(IPTC.ARTWORK_OR_OBJECT_DETAIL_SOURCE, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.ARTWORK_OR_OBJECT_DETAIL_SOURCE_INVENTORY_NUMBER
		this.addMapping(IPTC.ARTWORK_OR_OBJECT_DETAIL_TITLE, new TextPropertyMD(IPTC.ARTWORK_OR_OBJECT_DETAIL_TITLE, DCTerms.ALTERNATIVE, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(IPTC.CITY, new TextPropertyMD(IPTC.CITY, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.CONTACT_INFO_ADDRESS
		// this.addMapping(IPTC.CONTACT_INFO_CITY
		// this.addMapping(IPTC.CONTACT_INFO_COUNTRY
		this.addMapping(IPTC.CONTACT_INFO_EMAIL, new TextPropertyMD(IPTC.CONTACT_INFO_EMAIL, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.CONTACT_INFO_PHONE
		// this.addMapping(IPTC.CONTACT_INFO_POSTAL_CODE
		// this.addMapping(IPTC.CONTACT_INFO_STATE_PROVINCE
		// this.addMapping(IPTC.CONTACT_INFO_WEB_URL
		this.addMapping(IPTC.CONTROLLED_VOCABULARY_TERM, new TextPropertyMD(IPTC.CONTROLLED_VOCABULARY_TERM, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.COPYRIGHT_NOTICE, new TextPropertyMD(IPTC.COPYRIGHT_NOTICE, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.COPYRIGHT_OWNER, new TextPropertyMD(IPTC.COPYRIGHT_OWNER, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(IPTC.COPYRIGHT_OWNER_ID
		this.addMapping(IPTC.COPYRIGHT_OWNER_NAME, new TextPropertyMD(IPTC.COPYRIGHT_OWNER_NAME, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(IPTC.COUNTRY
		// this.addMapping(IPTC.COUNTRY_CODE
		this.addMapping(IPTC.CREATOR, new TextPropertyMD(IPTC.CREATOR, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.CREATORS_CONTACT_INFO
		// this.addMapping(IPTC.CREATORS_JOB_TITLE
		this.addMapping(IPTC.CREDIT_LINE, new TextPropertyMD(IPTC.CREDIT_LINE, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.DATE_CREATED, new TextPropertyMD(IPTC.DATE_CREATED, DCTerms.CREATED, DCTerms.PREFERRED_PREFIX, false, true));
		this.addMapping(IPTC.DESCRIPTION, new TextPropertyMD(IPTC.DESCRIPTION, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.DESCRIPTION_WRITER, new TextPropertyMD(IPTC.DESCRIPTION_WRITER, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.DIGITAL_IMAGE_GUID, new TextPropertyMD(IPTC.DIGITAL_IMAGE_GUID, DublinCore.IDENTIFIER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.DIGITAL_SOURCE_TYPE
		this.addMapping(IPTC.EVENT, new TextPropertyMD(IPTC.EVENT, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.HEADLINE, new TextPropertyMD(IPTC.HEADLINE, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.IMAGE_CREATOR, new TextPropertyMD(IPTC.IMAGE_CREATOR, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.IMAGE_CREATOR_ID
		this.addMapping(IPTC.IMAGE_CREATOR_NAME, new TextPropertyMD(IPTC.IMAGE_CREATOR_NAME, DublinCore.CREATOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.IMAGE_REGISTRY_ENTRY
		this.addMapping(IPTC.IMAGE_SUPPLIER, new TextPropertyMD(IPTC.IMAGE_SUPPLIER, DublinCore.PUBLISHER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.IMAGE_SUPPLIER_ID
		// this.addMapping(IPTC.IMAGE_SUPPLIER_IMAGE_ID
		this.addMapping(IPTC.IMAGE_SUPPLIER_NAME, new TextPropertyMD(IPTC.IMAGE_SUPPLIER_NAME, DublinCore.PUBLISHER_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.INSTRUCTIONS
		this.addMapping(IPTC.INTELLECTUAL_GENRE, new TextPropertyMD(IPTC.INTELLECTUAL_GENRE, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.IPTC_LAST_EDITED, new TextPropertyMD(IPTC.IPTC_LAST_EDITED, DCTerms.MODIFIED, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(IPTC.JOB_ID
		this.addMapping(IPTC.KEYWORDS, new TextPropertyMD(IPTC.KEYWORDS, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.LICENSOR, new TextPropertyMD(IPTC.LICENSOR, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(IPTC.LICENSOR_CITY
		// this.addMapping(IPTC.LICENSOR_COUNTRY
		this.addMapping(IPTC.LICENSOR_EMAIL, new TextPropertyMD(IPTC.LICENSOR_EMAIL, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(IPTC.LICENSOR_EXTENDED_ADDRESS
		// this.addMapping(IPTC.LICENSOR_ID
		this.addMapping(IPTC.LICENSOR_NAME, new TextPropertyMD(IPTC.LICENSOR_NAME, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		// this.addMapping(IPTC.LICENSOR_POSTAL_CODE
		// this.addMapping(IPTC.LICENSOR_REGION
		// this.addMapping(IPTC.LICENSOR_STREET_ADDRESS
		// this.addMapping(IPTC.LICENSOR_TELEPHONE_1
		// this.addMapping(IPTC.LICENSOR_TELEPHONE_2
		this.addMapping(IPTC.LICENSOR_URL, new TextPropertyMD(IPTC.LICENSOR_URL, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(IPTC.LOCATION_CREATED, new TextPropertyMD(IPTC.LOCATION_CREATED, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.LOCATION_CREATED_CITY
		// this.addMapping(IPTC.LOCATION_CREATED_COUNTRY_CODE
		// this.addMapping(IPTC.LOCATION_CREATED_COUNTRY_NAME
		// this.addMapping(IPTC.LOCATION_CREATED_PROVINCE_OR_STATE
		// this.addMapping(IPTC.LOCATION_CREATED_SUBLOCATION
		// this.addMapping(IPTC.LOCATION_CREATED_WORLD_REGION
		this.addMapping(IPTC.LOCATION_SHOWN, new TextPropertyMD(IPTC.LOCATION_SHOWN, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.LOCATION_SHOWN_CITY
		// this.addMapping(IPTC.LOCATION_SHOWN_COUNTRY_NAME
		// this.addMapping(IPTC.LOCATION_SHOWN_PROVINCE_OR_STATE
		// this.addMapping(IPTC.LOCATION_SHOWN_SUBLOCATION
		// this.addMapping(IPTC.LOCATION_SHOWN_WORLD_REGION
		// this.addMapping(IPTC.MAX_AVAIL_HEIGHT
		// this.addMapping(IPTC.MAX_AVAIL_WIDTH
		// this.addMapping(IPTC.MINOR_MODEL_AGE_DISCLOSURE
		// this.addMapping(IPTC.MODEL_AGE
		// this.addMapping(IPTC.MODEL_RELEASE_ID
		// this.addMapping(IPTC.MODEL_RELEASE_STATUS
		// this.addMapping(IPTC.ORGANISATION_CODE
		this.addMapping(IPTC.ORGANISATION_NAME, new TextPropertyMD(IPTC.ORGANISATION_NAME, DublinCore.CONTRIBUTOR_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.PERSON, new TextPropertyMD(IPTC.PERSON, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		// this.addMapping(IPTC.PLUS_VERSION
		// this.addMapping(IPTC.PROPERTY_RELEASE_ID
		// this.addMapping(IPTC.PROPERTY_RELEASE_STATUS
		// this.addMapping(IPTC.PROVINCE_OR_STATE
		// this.addMapping(IPTC.REGISTRY_ENTRY_CREATED_ITEM_ID
		// this.addMapping(IPTC.REGISTRY_ENTRY_CREATED_ORGANISATION_ID
		this.addMapping(IPTC.RIGHTS_USAGE_TERMS, new TextPropertyMD(IPTC.RIGHTS_USAGE_TERMS, DublinCore.RIGHTS_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.SCENE_CODE, new TextPropertyMD(IPTC.SCENE_CODE, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.SOURCE, new TextPropertyMD(IPTC.SOURCE, DCTerms.RIGHTS_HOLDER, DCTerms.PREFERRED_PREFIX, true, false));
		this.addMapping(IPTC.SUBJECT_CODE, new TextPropertyMD(IPTC.SUBJECT_CODE, DublinCore.SUBJECT_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.SUBLOCATION, new TextPropertyMD(IPTC.SUBLOCATION, DublinCore.DESCRIPTION_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, true, false));
		this.addMapping(IPTC.TITLE, new TextPropertyMD(IPTC.TITLE, DublinCore.TITLE_PROPERTY_NAME, DefaultMetaDataWriter.PREFIX_DC, false, true));

		this.addMapping(Geographic.LATITUDE, new RealPropertyMD(Geographic.LATITUDE, DefaultMetaDataWriter.WGS_LAT, DefaultMetaDataWriter.PREFIX_WGS, false, true));
		this.addMapping(Geographic.LONGITUDE, new RealPropertyMD(Geographic.LONGITUDE, DefaultMetaDataWriter.WGS_LONG, DefaultMetaDataWriter.PREFIX_WGS, false, true));
		// this.addMapping(Geographic.ALTITUDE

		/*
		 * A lot of other metadata classes exist but are not mapped because:
		 * - either no target property exist in "our" ontologies,
		 * - or these are still String and not Property in Tika,
		 * - are composite and in fact already mapped.
		 *
		 *
		 * - AccessPermissions
		 * - ClimateForcast
		 * - CreativeCommons
		 * - Database
		 * - Photoshop
		 * - RTFMetadata
		 * - TikaMetadataKeys
		 * - XMPIdq
		 * - XMPMM
		 * - TikaCoreProperties
		 */
	}

}
