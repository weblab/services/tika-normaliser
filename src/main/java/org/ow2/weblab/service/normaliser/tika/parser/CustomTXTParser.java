/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ow2.weblab.service.normaliser.tika.parser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.tika.config.ServiceLoader;
import org.apache.tika.detect.AutoDetectReader;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.txt.TXTParser;
import org.apache.tika.sax.XHTMLContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;



/**
 * This class is a custom version of original Tika's {@link TXTParser}.
 * The main difference is the fact that this version generates one paragraph at each newline, thus preserving newlines in the output document.
 *
 * Plain text parser. The text encoding of the document stream is automatically detected based on the byte patterns found at the beginning of the stream and the given document metadata, most notably
 * the <code>charset</code> parameter of a {@link org.apache.tika.metadata.HttpHeaders#CONTENT_TYPE} value.
 * <p>
 * This parser sets the following output metadata entries:
 * <dl>
 * <dt>{@link org.apache.tika.metadata.HttpHeaders#CONTENT_TYPE}</dt>
 * <dd><code>text/plain; charset=...</code></dd>
 * </dl>
 *
 */
public class CustomTXTParser extends org.apache.tika.parser.txt.TXTParser {


	private static final long serialVersionUID = 9134071836097702504L;


	private static final ServiceLoader LOADER = new ServiceLoader(TXTParser.class.getClassLoader());


	@Override
	public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext ctx) throws IOException, SAXException, TikaException {
		// Automatically detect the character encoding
		try (AutoDetectReader reader = new AutoDetectReader(new CloseShieldInputStream(stream), metadata, ctx.get(ServiceLoader.class, CustomTXTParser.LOADER));) {
			final Charset charset = reader.getCharset();
			final MediaType type = new MediaType(MediaType.TEXT_PLAIN, charset);
			metadata.set(HttpHeaders.CONTENT_TYPE, type.toString());
			// deprecated, see TIKA-431
			metadata.set(HttpHeaders.CONTENT_ENCODING, charset.name());

			final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
			xhtml.startDocument();
			String line = reader.readLine();
			while (line != null) {
				if (line.trim().isEmpty()) {
					xhtml.startElement("br");
					xhtml.endElement("br");
				}
				xhtml.startElement("p");
				xhtml.characters(line);
				xhtml.endElement("p");
				line = reader.readLine();
			}
			xhtml.endDocument();
		}
	}

}
