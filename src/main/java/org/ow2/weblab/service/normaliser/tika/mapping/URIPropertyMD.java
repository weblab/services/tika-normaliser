/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.mapping;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Property.ValueType;
import org.ow2.weblab.service.normaliser.tika.Constants;


/**
 * This class defines how Tika's property having an URI as ValueType should be written in the WebLab document.
 *
 * @author ymombrun
 */
public class URIPropertyMD extends MappingDescription<URI> {


	public URIPropertyMD(final Property property, final String uri, final String prefix, final boolean multivalue, final boolean skipIfExisting) {
		super(property, uri, prefix, multivalue, skipIfExisting, URI.class);
		if (property.getValueType() != ValueType.URI) {
			final String message = "Property " + property.getName() + " is not URI based. It is a " + property.getValueType() + ".";
			throw new IllegalArgumentException("'<"+Constants.SERVICE_NAME+">'"+" Error processing the document. "+message);
		}
	}


	@Override
	protected Set<URI> extractPropertyValues(final Metadata metadata) {
		final String[] values = metadata.getValues(this.property);
		if ((values == null) || (values.length == 0)) {
			return Collections.emptySet();
		}

		final Set<URI> filteredValues = new HashSet<>();
		for (final String value : values) {
			if (value == null || value.length() < 2) {
				continue;
			}
			final URI theUriValue;
			try {
				theUriValue = new URI(value);
			} catch (final URISyntaxException urise) {
				this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process the document. "+"URI property " + this.property.getName() + " extracted by Tika but not in the expected format. Ignoring the value (" + value + ")", urise);
				continue;
			}
			if (!this.containsSimilarValue(theUriValue, filteredValues)) {
				filteredValues.add(theUriValue);
			}

		}
		return filteredValues;
	}


	@Override
	protected boolean compareSimilarValues(final URI value1, final URI value2) {
		return value1.toASCIIString().equals(value2.toASCIIString());
	}

}
