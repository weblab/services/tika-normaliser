/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.mapping;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Property.ValueType;
import org.ow2.weblab.service.normaliser.tika.Constants;



/**
 * This class defines how Tika's property having a Date as ValueType should be written in the Jena PoK Helper.
 *
 * @author ymombrun
 */
public class DatePropertyMD extends MappingDescription<Date> {


	public DatePropertyMD(final Property property, final String uri, final String prefix, final boolean multivalue, final boolean skipIfExisting) {
		super(property, uri, prefix, multivalue, skipIfExisting, Date.class);
		if (property.getValueType() != ValueType.DATE) {
			final String message = "Property " + property.getName() + " is not Date based. It is a " + property.getValueType() + ".";
			throw new IllegalArgumentException("'<"+Constants.SERVICE_NAME+">'"+" Error processing the document. "+message);
		}
	}


	@Override
	protected Set<Date> extractPropertyValues(final Metadata metadata) {
		final Date date = metadata.getDate(this.property);
		if (date == null) {
			return Collections.emptySet();
		}
		return Collections.singleton(date);
	}


	@Override
	protected boolean compareSimilarValues(final Date value1, final Date value2) {
		return DateUtils.truncatedEquals(value1, value2, Calendar.MINUTE);
	}

}
