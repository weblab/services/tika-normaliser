/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.normaliser.tika;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ResourceBundle;

import javax.jws.WebService;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.HttpHeaders;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.CompositeParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.TeeContentHandler;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.messages.Messages;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.service.normaliser.tika.handlers.WebLabHandlerDecorator;
import org.ow2.weblab.service.normaliser.tika.metadatawriter.MetadataWriter;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * <code>TikaExtractorService</code> is realisation of the {@link Analyser} interface.
 *
 * It is a normaliser, in charge of the conversion of a native file (html, word, pdf...) into a WebLab {@link Document} with extraction of the metadata and the text content. To do that it uses the
 * well known {@link Tika}
 *
 * It takes as an input an almost empty WebLab {@link Document} that is annotated with an <code>hasNativeContent</code> property that links to the native file to be read.
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class TikaExtractorService implements Analyser {


	/**
	 * The logger to be used inside this class.
	 */
	protected final Log logger;


	/**
	 * The resource bundle used when logging in this class.
	 */
	protected final ResourceBundle bundle;


	/**
	 * The <code>ContentManager</code> to use. Various implementation exists.
	 * They are defined through a configuration file.
	 */
	final protected ContentManager contentManager;


	/**
	 * The configuration to be used for the service.
	 */
	protected final TikaConfiguration serviceConfig;


	/**
	 * The configuration Tika by it self.
	 */
	protected final TikaConfig tikaConfig;


	/**
	 * MetadataWriter used
	 */
	protected final MetadataWriter metadataWriter;


	/**
	 * The only constructor of this class that needs a configuration.
	 *
	 * @param conf
	 *            The service configuration.
	 *
	 * @throws IOException
	 *             If an error occurs accessing the tika configuration or
	 *             instanciating the content manager.
	 * @throws TikaException
	 *             If an error occurs reading the tika configuration.
	 */
	public TikaExtractorService(final TikaConfiguration conf) throws TikaException, IOException {
		this.logger = LogFactory.getLog(this.getClass());
		this.bundle = ResourceBundle.getBundle(this.getClass().getCanonicalName());
		this.serviceConfig = conf;
		this.contentManager = ContentManager.getInstance();

		if (this.serviceConfig.getPathToXmlConfigurationFile() == null) {
			this.logger.debug(Messages.getString(this.bundle, Constants.KEY_DEBUG_DEFAULT_TIKA_CONFIG));
			this.tikaConfig = new TikaConfig();
		} else {
			this.logger.debug("Try to load Tika's configuration from " + this.serviceConfig.getPathToXmlConfigurationFile() + ".");
			try {
				this.tikaConfig = new TikaConfig(this.getClass().getClassLoader().getResource(this.serviceConfig.getPathToXmlConfigurationFile()));
			} catch (final Exception e) {
				final String message = Messages.getString(this.bundle, Constants.KEY_ERROR_CANNOT_LOAD_TIKA_CONFIG_1, this.serviceConfig.getPathToXmlConfigurationFile());
				this.logger.error(message, e);
				throw new IOException(message, e);
			}
		}

		if (!(this.tikaConfig.getParser() instanceof CompositeParser)) {
			this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_NOT_A_COMPOSITE_PARSER_1, this.tikaConfig.getParser().getClass().getCanonicalName()));
		}

		this.metadataWriter = conf.getMetadataWriter();

		this.logger.info(Messages.getString(this.bundle, Constants.KEY_INFO_SERVICE_STARTED));
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws InvalidParameterException, ContentNotAvailableException, UnexpectedException {
		// Check that the processArgs contains a document and return it.
		final Document document = this.checkArgs(args);

		final String source = new DublinCoreAnnotator(document).readSource().firstTypedValue();
		final String resourceUri = document.getUri();
		this.logger.debug(Messages.getString(this.bundle, Constants.KEY_INFO_PROCESS_DOCUMENT_1, resourceUri, source));

		// Check that the document contains a file in content manager and return it.
		final boolean tempFile;
		File file = this.getLocalContent(document);
		if (file == null) {
			file = this.getTempContent(document);
			tempFile = true;
		} else {
			tempFile = false;
		}

		try {
			// Feed the document with Text unit from file content and put metadata in the map.
			Metadata extractedMeta_l = this.extractTextAndMetadata(document, file, false);

			// If no text unit are extracted from document, try to extract once again but with the auto-detect parser.
			if (ResourceUtil.getSelectedSubResources(document, Text.class).isEmpty()) {
				this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_NO_TEXT_FOUND_2, file.getAbsolutePath(), resourceUri));
				extractedMeta_l = this.extractTextAndMetadata(document, file, true);
			}

			// Annotate the document with content of the map.
			if (this.serviceConfig.isAddMetadata()) {
				this.metadataWriter.write(extractedMeta_l, document, this.serviceConfig.getServiceUri());

				if (this.serviceConfig.isAddMissingContentFormat()) {
					final URI contentUri = new WProcessingAnnotator(document).readNativeContent().firstTypedValue();
					final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);
					dca.startInnerAnnotatorOn(contentUri);
					if (!dca.readFormat().hasValue()) {
						final String mime = extractedMeta_l.get(HttpHeaders.CONTENT_TYPE);
						if (mime != null) {
							final MediaType format = MediaType.parse(mime);
							if (format != null) {
								dca.writeFormat(format.getBaseType().toString());
							}
						}
					}
				}
			}
		} finally {
			// Remove temporary content file if needed
			if (tempFile && this.serviceConfig.isRemoveTempContent() && !file.delete()) {
				this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_UNABLE_TO_DELETE_TEMP_2, file.getAbsolutePath(), resourceUri));
				file.deleteOnExit();
			}
		}



		// Create the return wrapper.
		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(document);

		this.logger.debug(Messages.getString(this.bundle, Constants.KEY_INFO_PROCESS_DOCUMENT_1, resourceUri, source));

		return pr;
	}


	/**
	 * @param document
	 *            The input document to be processed
	 * @return The local file or <code>null</code> if {@link ContentManager#readLocalExistingFile(URI, Resource, java.util.Map)} returns <code>null</code>
	 * @throws ContentNotAvailableException
	 *             If no native content is annotated on document
	 */
	protected File getLocalContent(final Document document) throws ContentNotAvailableException {
		final Value<URI> values = new WProcessingAnnotator(document).readNativeContent();
		if (!values.hasValue()) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_CONTENT_NOT_AVAILABLE_1, document.getUri());
			this.logger.error(err);
			throw ExceptionFactory.createContentNotAvailableException(err);
		}

		final File localFile = this.contentManager.readLocalExistingFile(values.firstTypedValue(), null, null);
		return localFile;
	}


	/**
	 * Get the document inside the process args or throw an <code>InvalidParameterException</code> if not possible.
	 *
	 * @param args
	 *            The <code>ProcessArgs</code> of the process method.
	 * @return The <code>Document</code> that must be contained by <code>args</code>.
	 * @throws InvalidParameterException
	 *             If <code>resource</code> in <code>args</code> is <code>null</code> or not a <code>Document</code>.
	 */
	protected Document checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_PROCESSARGS_NULL);
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+err);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_RESOURCE_NULL);
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+err);
		}
		if (!(res instanceof Document)) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_NOT_A_DOCUMENT_2, res.getUri(), res.getClass().getCanonicalName());
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException("'<"+Constants.SERVICE_NAME+">'"+" Error processing document. "+err);
		}
		return (Document) res;
	}


	/**
	 * Uses the content manager to retrieve the native content of the document in input.
	 *
	 * @param document
	 *            The document that must contains an hasNativeContentProperty
	 * @return The temp file resulting of the stream copy coming from the content manager.
	 * @throws ContentNotAvailableException
	 *             If the required property is missing, if ContentManager fails opening the stream or if an error occurs with stream copy.
	 */
	private File getTempContent(final Document document) throws ContentNotAvailableException {
		final Value<URI> values = new WProcessingAnnotator(document).readNativeContent();
		final File tempFile;
		try (InputStream fis = this.contentManager.read(values.firstTypedValue())) {
			tempFile = File.createTempFile("tika.", ".content");
			FileUtils.copyInputStreamToFile(fis, tempFile);
		} catch (final IOException ioe) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_CONTENT_NOT_AVAILABLE_1, document.getUri());
			this.logger.error(err, ioe);
			throw ExceptionFactory.createContentNotAvailableException(err, ioe);
		}
		if (!tempFile.exists()) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_CONTENT_FILE_NOT_FOUND_2, tempFile.getAbsolutePath(), document.getUri());
			this.logger.error(err);
			throw ExceptionFactory.createContentNotAvailableException(err);
		}
		if (!tempFile.canRead()) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_CONTENT_FILE_NOT_READABLE_2, tempFile.getAbsolutePath(), document.getUri());
			this.logger.error(err);
			throw ExceptionFactory.createContentNotAvailableException(err);
		}
		return tempFile;
	}


	/**
	 * @param document
	 *            The document to be fill with MediaUnit units
	 * @param contentFile
	 *            The file to be parsed
	 * @param forceAutoDetectParser
	 *            Whether to let Tika guess the parser to use from file content or use existing mimeType on the document (dc:format) to select the appropriated parser.
	 * @return The metadata map enriched
	 * @throws UnexpectedException
	 *             If the Tika parser fails.
	 * @throws ContentNotAvailableException
	 *             If the file is not reachable. (This should not appear this its access has been checked before)
	 */
	protected Metadata extractTextAndMetadata(final Document document, final File contentFile, final boolean forceAutoDetectParser) throws UnexpectedException, ContentNotAvailableException {
		// Try to get mimeType in media unit if forceAutoDetectParser condition is false
		final String mimeType;
		if (forceAutoDetectParser) {
			mimeType = null;
		} else {
			final Value<String> format = new DublinCoreAnnotator(document).readFormat();
			if (format.hasValue()) {
				mimeType = format.firstTypedValue();
				if (format.getValues().size() > 1) {
					this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_MORE_THAN_ONE_TYPE_2, document.getUri(), mimeType));
				}
			} else {
				mimeType = null;
			}
			this.logger.debug("Mime type detected in Resource: " + mimeType);
		}


		// If the mime type not defined, uses the auto-detect parser. Otherwise, look up in the Tika config to get the appropriated parser.
		final Parser parser;
		if (mimeType == null) {
			parser = new AutoDetectParser(this.tikaConfig);
		} else if (this.tikaConfig.getParser() instanceof CompositeParser) {
			final CompositeParser composite = (CompositeParser) this.tikaConfig.getParser();
			final MediaType mediaType = MediaType.parse(mimeType);
			if (composite.getParsers().containsKey(mediaType)) {
				parser = composite.getParsers().get(mediaType);
			} else {
				this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_NO_PARSER_FOR_TYPE_1, mediaType));
				parser = new AutoDetectParser(this.tikaConfig);
			}
		} else {
			parser = this.tikaConfig.getParser();
			// The Parser in the configuration is not composite. That's weird? We only parse one type of file?
			this.logger.debug("Tika Config does not use an AutodetectParser but a " + parser.getClass().getCanonicalName() + ".");
		}

		/*
		 * Create an xhtmlOutput file in the temp directory (even if not used).
		 * The variable generateHtml is used to keep track on errors. Will be true only if at the end of the method the content has been generated.
		 */
		boolean generateHtml = this.serviceConfig.isGenerateHtml();

		File xhtmlOutputFile;
		if (generateHtml) {
			try {
				xhtmlOutputFile = File.createTempFile("tika", ".xhtml");
			} catch (final IOException ioe) {
				this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_UNABLE_TO_CREATE_TEMP_FILE_1, document.getUri()), ioe);
				xhtmlOutputFile = new File(FileUtils.getTempDirectory(), "noFile");
				generateHtml = false;
			}
		} else {
			xhtmlOutputFile = null;
		}

		// The metadata object to be filled by Tika parser.
		final Metadata metadata = new Metadata();
		try {
			/*
			 * Create the appropriated handler (or tee handler) depending on the things needed. --> MediaUnit + Normalised content generator or MediaUnit only.
			 *
			 * If an error occurs creating the transformer for normalised content generator, it is just skipped and the generateHtml variable is set to false to prevent use of an empty content.
			 */
			ContentHandler handler;
			if (generateHtml) {
				try {
					handler = new TeeContentHandler(this.getMUCreatorCHandler(document), TikaExtractorService.getHtmlCreatorCHandler(xhtmlOutputFile));
				} catch (final TransformerConfigurationException tce) {
					this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_UNABLE_TO_CREATE_TRANSFORMER_1, document.getUri()), tce);
					generateHtml = false;
					handler = this.getMUCreatorCHandler(document);
				}
			} else {
				handler = this.getMUCreatorCHandler(document);
			}

			// The parsecontext
			final ParseContext context = new ParseContext();

			// The parse original file
			this.logger.debug("Start parsing " + contentFile.getPath() + " for document " + document.getUri() + ".");
			try (final InputStream stream = new FileInputStream(contentFile);) {
				parser.parse(stream, handler, metadata, context);
			} catch (final FileNotFoundException fnfe) {
				final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_CONTENT_FILE_NOT_FOUND_2, contentFile.getAbsolutePath(), document.getUri());
				this.logger.error(err);
				throw ExceptionFactory.createContentNotAvailableException(err, fnfe);
			} catch (final IOException ioe) {
				final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_IOE_ON_CONTENT_2, contentFile.getPath(), document.getUri());
				this.logger.error(err, ioe);
				throw ExceptionFactory.createUnexpectedException(err, ioe);
			} catch (final SAXException saxe) {
				final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_SAXE_ON_CONTENT_2, contentFile.getPath(), document.getUri());
				this.logger.error(err, saxe);
				throw ExceptionFactory.createUnexpectedException(err, saxe);
			} catch (final TikaException te) {
				final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_TIKA_EX_ON_CONTENT_2, contentFile.getPath(), document.getUri());
				this.logger.error(err, te);
				throw ExceptionFactory.createUnexpectedException(err, te);
			}
			this.logger.debug("Finished parsing " + contentFile.getPath() + " for document " + document.getUri() + ".");

			if (generateHtml && (xhtmlOutputFile != null)) {
				if (!xhtmlOutputFile.exists()) {
					this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_NO_OUTPUT_FILE_2, xhtmlOutputFile.getPath(), document.getUri()));
				} else if (xhtmlOutputFile.length() <= 0) {
					this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_EMPTY_OUTPUT_FILE_2, xhtmlOutputFile.getPath(), document.getUri()));
				} else {
					try (final FileInputStream fis = new FileInputStream(xhtmlOutputFile)) {
						this.logger.debug("Save normalised content file: " + xhtmlOutputFile);
						final URI normalisedContent = this.contentManager.create(fis);
						final PieceOfKnowledge pok = new WProcessingAnnotator(document).writeNormalisedContent(normalisedContent);
						new DublinCoreAnnotator(normalisedContent, pok).writeFormat("text/html");
					} catch (final FileNotFoundException fnfe) {
						this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_NO_OUTPUT_FILE_2, xhtmlOutputFile.getPath(), document.getUri()), fnfe);
					} catch (final IOException ioe) {
						this.logger.warn(Messages.getString(this.bundle, Constants.KEY_WARN_ERROR_SAVING_NORMALISED_2, xhtmlOutputFile.getPath(), document.getUri()), ioe);
					}
				}
			}
		} finally {
			if ((xhtmlOutputFile != null) && !FileUtils.deleteQuietly(xhtmlOutputFile)) {
				xhtmlOutputFile.deleteOnExit();
			}
		}

		return metadata;
	}


	/**
	 * Creates a new MediaUnit content Handler, with a HTML Body handler inside.
	 *
	 * @param document
	 *            The document to be enriched with mediaUnits
	 * @return The MediaUnitContent Handler
	 * @throws UnexpectedException
	 *             If the decorator class cannot be instantiated
	 */
	private WebLabHandlerDecorator getMUCreatorCHandler(final Document document) throws UnexpectedException {
		final WebLabHandlerDecorator wlhd;
		try {
			wlhd = this.serviceConfig.getWebLabHandlerDecoratorClass().newInstance();
		} catch (final Exception e) {
			final String err = Messages.getString(this.bundle, Constants.KEY_ERROR_BAD_HANDLER_1, this.serviceConfig.getWebLabHandlerDecoratorClass().getCanonicalName());
			this.logger.error(err, e);
			throw ExceptionFactory.createUnexpectedException(err, e);
		}
		wlhd.setDocument(document);
		wlhd.setTikaConfiguration(this.serviceConfig);
		wlhd.setContentHandler(new BodyContentHandler(-1));
		return wlhd;
	}


	/**
	 * Creates an handler in charge of writing the XHTML events into the xhtmlFile.
	 *
	 * @param xhtmlFile
	 *            The file in which the XHTML should be written.
	 * @return A ContentHandler that writes into the file
	 * @throws TransformerConfigurationException
	 *             If the content handler cannot be created.
	 */
	private static ContentHandler getHtmlCreatorCHandler(final File xhtmlFile) throws TransformerConfigurationException {
		final SAXTransformerFactory factory = (SAXTransformerFactory) TransformerFactory.newInstance();
		final TransformerHandler handler;
		handler = factory.newTransformerHandler();
		handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "xml");
		handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
		handler.setResult(new StreamResult(xhtmlFile));
		return handler;
	}

}
