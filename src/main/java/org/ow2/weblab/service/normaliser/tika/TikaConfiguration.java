/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika;

import java.net.URI;

import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.service.normaliser.tika.handlers.SimpleTextContentHandler;
import org.ow2.weblab.service.normaliser.tika.handlers.WebLabHandlerDecorator;
import org.ow2.weblab.service.normaliser.tika.metadatawriter.DefaultMetaDataWriter;
import org.ow2.weblab.service.normaliser.tika.metadatawriter.MetadataWriter;

/**
 * This class is just an holder for the configuration of the Tika Service.
 *
 * @author WebLal IPCC Team
 */
public class TikaConfiguration {


	/**
	 * The default weblabHandlerDecorator
	 */
	public final static Class<? extends WebLabHandlerDecorator> WEBLAB_HANDLER_DECORATOR = SimpleTextContentHandler.class;


	/**
	 * Path to tika XML configuration file. If null, default TikaConfig is used.
	 */
	private String pathToXmlConfigurationFile;


	/**
	 * Whether to annotate the document with metadata guessed by Tika.
	 */
	private boolean addMetadata;


	/**
	 * Whether or not to generate an HTML pivot representation of the content.
	 * It will be stored using the method {@link ContentManager#writeNormalisedContent(java.io.InputStream, org.ow2.weblab.core.model.Resource)}.
	 */
	private boolean generateHtml;


	/**
	 * Whether or not to generate to annotate native content with detected mime type if absent.
	 */
	private boolean addMissingContentFormat;


	/**
	 * If the content is a temporary one (the local copy after a transfer through FTP or WebDAV for instance), whether to remove it after a successful process or not.
	 */
	private boolean removeTempContent;


	/**
	 * The service URI to be used to annotate the created annotations. If null, nothing will be done.
	 */
	private URI serviceUri;


	/**
	 * The class of the WebLab Handler Decorator that will be instantiated to convert the HTML into a WebLab document.
	 *
	 * If <code>null</code> the default value is {@value #WEBLAB_HANDLER_DECORATOR}
	 */
	private Class<? extends WebLabHandlerDecorator> webLabHandlerDecoratorClass;


	/**
	 * The instance of MetadataWriter that will be used to write Tika metadata.
	 *
	 * If <code>null</code> the default value is {@linkplain DefaultMetaDataWriter}
	 */
	private MetadataWriter metadataWriter;


	/**
	 * The constructor
	 */
	public TikaConfiguration() {
		super();
	}


	/**
	 *
	 * @return the path to xml tika config file
	 */
	public String getPathToXmlConfigurationFile() {
		return this.pathToXmlConfigurationFile;
	}


	/**
	 *
	 * @param pathToXmlConfigurationFile
	 *            the path to xml tika config file
	 */
	public void setPathToXmlConfigurationFile(final String pathToXmlConfigurationFile) {
		this.pathToXmlConfigurationFile = pathToXmlConfigurationFile;
	}


	/**
	 *
	 * @param serviceUri
	 *            the service URI (used to annotated the annotations)
	 */
	public void setServiceUri(final URI serviceUri) {
		this.serviceUri = serviceUri;
	}


	/**
	 * @return the serviceUri
	 */
	public URI getServiceUri() {
		if (this.serviceUri == null) {
			return null;
		}
		return this.serviceUri;
	}


	/**
	 * @return the webLabHandlerDecoratorClass
	 */
	public Class<? extends WebLabHandlerDecorator> getWebLabHandlerDecoratorClass() {
		if (this.webLabHandlerDecoratorClass == null) {
			return TikaConfiguration.WEBLAB_HANDLER_DECORATOR;
		}
		return this.webLabHandlerDecoratorClass;
	}


	/**
	 * @return the addMetadata
	 */
	public boolean isAddMetadata() {
		return this.addMetadata;
	}


	/**
	 * @return the generateHtml
	 */
	public boolean isGenerateHtml() {
		return this.generateHtml;
	}



	/**
	 * @return the removeTempContent
	 */
	public boolean isRemoveTempContent() {
		return this.removeTempContent;
	}


	/**
	 * @param addMetadata
	 *            the addMetadata to set
	 */
	public void setAddMetadata(final boolean addMetadata) {
		this.addMetadata = addMetadata;
	}


	/**
	 * @param generateHtml
	 *            the generateHtml to set
	 */
	public void setGenerateHtml(final boolean generateHtml) {
		this.generateHtml = generateHtml;
	}


	/**
	 * @param removeTempContent
	 *            the removeTempContent to set
	 */
	public void setRemoveTempContent(final boolean removeTempContent) {
		this.removeTempContent = removeTempContent;
	}


	/**
	 * @param serviceUri
	 *            the serviceUri to set
	 */
	public void setServiceUri(final String serviceUri) {
		if ((serviceUri == null) || serviceUri.isEmpty()) {
			this.serviceUri = null;
		} else {
			this.serviceUri = URI.create(serviceUri);
		}
	}


	/**
	 * @param webLabHandlerDecoratorClass
	 *            the webLabHandlerDecoratorClass to set
	 */
	public void setWebLabHandlerDecoratorClass(final Class<? extends WebLabHandlerDecorator> webLabHandlerDecoratorClass) {
		this.webLabHandlerDecoratorClass = webLabHandlerDecoratorClass;
	}


	/**
	 * @return the metadataWriter
	 */
	public synchronized MetadataWriter getMetadataWriter() {
		if (this.metadataWriter == null) {
			this.metadataWriter = new DefaultMetaDataWriter();
		}
		return this.metadataWriter;
	}


	/**
	 * @param metadataWriter
	 *            the metadataWriter to set
	 */
	public void setMetadataWriter(final MetadataWriter metadataWriter) {
		this.metadataWriter = metadataWriter;
	}


	/**
	 * @return the addMissingContentFormat
	 */
	public boolean isAddMissingContentFormat() {
		return this.addMissingContentFormat;
	}


	/**
	 * @param addMissingContentFormat
	 *            the addMissingContentFormat to set
	 */
	public void setAddMissingContentFormat(final boolean addMissingContentFormat) {
		this.addMissingContentFormat = addMissingContentFormat;
	}

}
