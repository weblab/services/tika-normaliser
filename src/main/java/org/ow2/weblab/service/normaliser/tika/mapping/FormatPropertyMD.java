/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.mapping;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.mime.MediaType;
import org.ow2.weblab.core.extended.ontologies.DublinCore;



/**
 * The dc:format is a specific property that has to be validated. Thus we have created a specific {@link MappingDescription}.
 *
 * @author ymombrun
 */
public class FormatPropertyMD extends TextPropertyMD {


	private static final String DC = "dc";


	public FormatPropertyMD(final Property property) {
		super(property, DublinCore.FORMAT_PROPERTY_NAME, FormatPropertyMD.DC, false, true);
	}


	@Override
	protected Set<String> extractPropertyValues(final Metadata metadata) {
		final String[] values = metadata.getValues(this.property);
		if ((values == null) || (values.length == 0)) {
			return Collections.emptySet();
		}
		final Set<String> filteredValues = new HashSet<>();
		for (final String value : values) {
			if (value == null || value.length() < 2) {
				continue;
			}
			final MediaType format = MediaType.parse(value);
			if (format == null) {
				continue;
			}
			final String formattedValue = format.getBaseType().toString();
			if (!this.containsSimilarValue(formattedValue, filteredValues)) {
				filteredValues.add(formattedValue);
			}
		}
		return filteredValues;
	}

}
