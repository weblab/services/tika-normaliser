/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.handlers;

import org.apache.tika.sax.ContentHandlerDecorator;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;
import org.xml.sax.ContentHandler;



/**
 * This class is an abstract handler decorator that should be specialised and instantiated inside the TikaService for the creation of WebLab document when parsing the Tika's XHTML output.
 *
 * It contains two methods that should be overridden (even if they are not really used). They are called by the TikaService when instantiating the Handler.
 *
 * @author WebLab IPCC Team
 */
public abstract class WebLabHandlerDecorator extends ContentHandlerDecorator {


	/**
	 * The default constructor
	 */
	public WebLabHandlerDecorator() {
		super();
	}


	/**
	 * Sets the document that should be enriched when processing the content with the handler.
	 *
	 * @param document
	 *            The WebLab document to be enriched
	 */
	public abstract void setDocument(final Document document);


	/**
	 * Sends the service configuration to this decorator. It will be called even if the configuration is not used inside.
	 *
	 * @param tikaConfiguration
	 *            The service configuration
	 */
	public abstract void setTikaConfiguration(final TikaConfiguration tikaConfiguration);


	@Override
	public void setContentHandler(final ContentHandler handler) {
		super.setContentHandler(handler);
	}

}
