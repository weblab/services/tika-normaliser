/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.handlers;

import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.service.normaliser.tika.TikaConfiguration;



/**
 * This is an empty implementation of the {@link WebLabHandlerDecorator}.
 * It does nothing and thus can be used when the service is used obnly to extract metadata.
 *
 * @author ymombrun
 */
public class EmptyContentHandler extends WebLabHandlerDecorator {


	@Override
	public void setDocument(final Document document) {
		// Nothing to do
	}


	@Override
	public void setTikaConfiguration(final TikaConfiguration tikaConfiguration) {
		// Nothing to do
	}

}
