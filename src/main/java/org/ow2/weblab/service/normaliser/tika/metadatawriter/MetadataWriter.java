/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.metadatawriter;

import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.service.normaliser.tika.Constants;
import org.ow2.weblab.service.normaliser.tika.mapping.MappingDescription;
import org.purl.dc.terms.DCTermsAnnotator;

/**
 * This class is the base class for MetadataWriters.
 *
 * It contains the mapping between metadata as found by Tika to Weblab ontology (or any ontology you'd like) and write these metadata in the Weblab resource according to the PokHelper given. As this
 * class does not contain any mapping, a subclass must be created.
 *
 * This subclass have to provide an implementation for {@link #init() init} method which basically makes calls to {@link #addMapping(Property, MappingDescription) addMapping} method.
 *
 * Various maps are containing specific kind of mappings, depending on the expected type of the value.
 *
 * @author Frédéric Praca, ymombrun
 *
 * @see org.apache.tika.metadata.Metadata
 */
public abstract class MetadataWriter {


	/**
	 * The logger to be used inside this class.
	 */
	protected final Log logger;


	/**
	 * Map to store the mapping between Property and the description on how they have to be annotated.
	 */
	private final Map<Property, List<MappingDescription<?>>> propertyMapping = new HashMap<>();


	/**
	 * The list of names of the properties mapped
	 */
	private final Set<String> mappedKeys = new HashSet<>();


	/**
	 * Default constructor
	 */
	public MetadataWriter() {
		super();
		this.logger = LogFactory.getLog(this.getClass());
		this.init();
		this.postInit();
	}



	/**
	 * Fills the set of mapped keys with the names of the properties that have been added to the mapping in {@link #init() init} method.
	 */
	private void postInit() {
		for (final Property property : this.propertyMapping.keySet()) {
			this.mappedKeys.add(property.getName());
		}
	}


	/**
	 * This method allows to write the Metadata created by Tika inside the correct part of the document. Each annotation is written to reference the URI of the document provided.
	 *
	 * @param metadataToWrite
	 *            The {@link org.apache.tika.metadata.Metadata Metadata} object to map and write
	 * @param document
	 *            The document to written annotation into
	 * @param serviceUri
	 *            The uri of the service or null (used to annotate or not the annotation with creation date and producer information)
	 */
	public void write(final Metadata metadataToWrite, final Resource document, final URI serviceUri) {
		final BaseAnnotator existingProperties = new BaseAnnotator(document);

		// Prepare annotation but remove it first (to ensure new properties are not considered as existing ones)
		final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(document);
		document.getAnnotation().remove(annot);
		final URI documentUri = URI.create(document.getUri());
		final BaseAnnotator annotator = new BaseAnnotator(documentUri, annot);

		boolean anythingWritten = false;
		for (final Entry<Property, List<MappingDescription<?>>> entry : this.propertyMapping.entrySet()) {
			for (final MappingDescription<?> mapping : entry.getValue()) {
				final boolean written = mapping.write(metadataToWrite, annotator, documentUri, existingProperties);
				anythingWritten = anythingWritten || written;
			}
		}

		if (anythingWritten) {
			if (serviceUri != null) {
				final URI annotUri = URI.create(annot.getUri());
				new WProcessingAnnotator(annotUri, annot).writeProducedBy(serviceUri);
				new DCTermsAnnotator(annotUri, annot).writeCreated(Calendar.getInstance().getTime());
			}
			document.getAnnotation().add(annot);
		} else {
			this.logger.warn("'<"+Constants.SERVICE_NAME+">'"+" Unable to properly process the document. "+" Not a single property extracted for document " + document.getUri() + ".");
		}
	}


	/**
	 * Method used by subclass in their {@link #init() init} method to add a mapping
	 *
	 * @param propertyToMap
	 *            the Tika property to map
	 * @param toDescription
	 *            the description to use for mapping.
	 */
	public void addMapping(final Property propertyToMap, final MappingDescription<?> toDescription) {
		if (!this.propertyMapping.containsKey(propertyToMap)) {
			this.propertyMapping.put(propertyToMap, new LinkedList<>());
		}
		this.propertyMapping.get(propertyToMap).add(toDescription);
	}


	/**
	 * Method to implement in subclass. Should basically be a set of calls to {@link #addMapping(Property, MappingDescription) addMapping}
	 */
	public abstract void init();

}
