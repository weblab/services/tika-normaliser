/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.mapping;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.Set;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Property.ValueType;



/**
 * A formatter that converts the Integer value of the property to a proper message.
 *
 * This is mainly used to handle singular vs plural.
 *
 * @author ymombrun
 */
public class MessageFormatMD extends MappingDescription<String> {


	private final MessageFormat messageFormat;


	public MessageFormatMD(final Property property, final String uri, final String prefix, final boolean multivalue, final boolean skipIfExisting, final String messageFormatPattern) {
		super(property, uri, prefix, multivalue, skipIfExisting, String.class);
		if (property.getValueType() != ValueType.INTEGER) {
			final String message = "Property " + property.getName() + " is not Integer based. It is a " + property.getValueType() + ".";
			this.logger.fatal(message);
			throw new IllegalArgumentException(message);
		}
		this.messageFormat = new MessageFormat(messageFormatPattern);
	}


	@Override
	protected Set<String> extractPropertyValues(final Metadata metadata) {
		final Integer integer = metadata.getInt(this.property);
		if (integer == null) {
			return Collections.emptySet();
		}
		return Collections.singleton(this.messageFormat.format(new Object[] { integer }));
	}


	@Override
	protected boolean compareSimilarValues(final String value1, final String value2) {
		return value1.equalsIgnoreCase(value2);
	}

}
