/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.mapping;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.service.normaliser.tika.metadatawriter.MetadataWriter;

/**
 * This abstract class aims to provide a mapping between a Tika metadata and an annotation represented by an URI.
 *
 * The specific implementations are in charge of defining the way the property value is retrieved from the Metadata holder and the way it is written in the annotation.
 *
 * @author Frédéric Praca, ymombrun
 * @param <T>
 *            The type of the value
 * @see org.apache.tika.metadata.Metadata
 * @see org.apache.tika.metadata.Property
 */
public abstract class MappingDescription<T> {


	/**
	 * The URI to be used in the RDF of the annotation
	 */
	protected final URI uri;


	/**
	 * The Tika property
	 */
	protected final Property property;


	/**
	 * The logger to be used inside this class.
	 */
	protected final Log logger;


	/**
	 * Whether or not the annotated document can have multiple values.
	 */
	protected boolean multiValue;


	/**
	 * Whether or not to annotate the document with the value if the document already has a value.
	 */
	protected boolean skipIfExisting;


	/**
	 * The String prefix to be used for the namespace of <code>uri</code>.
	 */
	protected final String prefix;


	/**
	 * The type of the value to be mapped
	 */
	protected final Class<T> type;


	/**
	 * Constructor that take a {@link Property XMP Property} as argument and the {@link URI uri} that should map it.
	 * It also takes as parameter two booleans that configures the multiple values and the behaviour to adopt when the value was already present on the document.
	 *
	 * For Tika metadata not represented with a Property object, it should be represented as Text property.
	 *
	 * @param property
	 *            the Tika property to map.
	 * @param uri
	 *            The String URI representing the mapping.
	 * @param prefix
	 *            The String prefix to be used for the namespace of <code>uri</code>.
	 * @param multiValue
	 *            Whether or not the annotated document can have multiple values.
	 * @param skipIfExisting
	 *            Whether or not to annotate the document with the value if the document already has a value.
	 * @param type
	 *            The type of the value to be mapped.
	 */
	public MappingDescription(final Property property, final String uri, final String prefix, final boolean multiValue, final boolean skipIfExisting, final Class<T> type) {
		this.logger = LogFactory.getLog(this.getClass());
		this.uri = URI.create(uri);
		this.prefix = prefix;
		this.property = property;
		this.multiValue = multiValue;
		this.skipIfExisting = skipIfExisting;
		this.type = type;
	}


	/**
	 * This method is a delegate for {@linkplain MetadataWriter#write(Metadata, Resource, URI)}.
	 *
	 * It has to be specialised by subclasses in order to customise the way values are retrieved and written.
	 *
	 * @param metadataToWrite
	 *            Metadata set to write
	 * @param baseAnnotator
	 *            The base annotator in charge of writing the metadata to the dedicated annotation
	 * @param documentUri
	 *            The document URI that will be used for reference
	 * @param existingProperties
	 *            An annotator opened on the original document enabling to read existing properties
	 * @return Whether or not anything has been added to pokHelperToWriteInto
	 */
	public boolean write(final Metadata metadataToWrite, final BaseAnnotator baseAnnotator, final URI documentUri, final BaseAnnotator existingProperties) {
		final Set<T> extractedValues = this.extractPropertyValues(metadataToWrite);
		if (extractedValues.isEmpty()) {
			return false;
		}
		final Set<T> existingValues = new HashSet<>(existingProperties.read(this.uri, this.type).getValues());
		if (!existingValues.isEmpty() && (this.skipIfExisting || !this.multiValue)) {
			this.logger.trace("Do not annotate " + this.property.getName() + " since a value for " + this.uri + " has been found on the WebLab document.");
			return false;
		}
		// Add the property that Tika has just written
		existingValues.addAll(baseAnnotator.read(this.uri, this.type).getValues());

		final Set<T> valuesToBeWritten = new HashSet<>();
		for (final T value : extractedValues) {
			if (this.containsSimilarValue(value, existingValues)) {
				this.logger.trace("The same value has already been annotated for " + this.property.getName() + ".");
				// Almost same value is already present on the document
			} else if (valuesToBeWritten.isEmpty()) {
				// Can't be a duplicate. Add it.
				valuesToBeWritten.add(value);
			} else if (!this.multiValue) {
				this.logger.trace("More than one value extracted by Tika for " + this.property.getName() + " while it is not multi-valued.");
				break;
			} else if (this.containsSimilarValue(value, valuesToBeWritten)) {
				this.logger.trace("The same value has already been extracted by Tika for " + this.property.getName() + ".");
				// Almost same value is already present in the selected values to be written
			} else {
				valuesToBeWritten.add(value);
			}
		}

		if (valuesToBeWritten.isEmpty()) {
			return false;
		}

		for (final T value : valuesToBeWritten) {
			baseAnnotator.write(this.prefix, this.uri, this.type, value);
		}

		return true;
	}


	/**
	 * @param metadata
	 *            The metadata object enriched by Tika and from which the vaue should be extracted
	 * @return A filtered list of values present in metadata. Any transformation applied on the value to write should be done here.
	 */
	protected abstract Set<T> extractPropertyValues(final Metadata metadata);


	/**
	 * @param value1
	 *            a value
	 * @param value2
	 *            another value
	 * @return Whether or not the two provided values are similar. Might not be a simple equals (for dates especially)
	 */
	protected abstract boolean compareSimilarValues(final T value1, final T value2);


	/**
	 * @param valueToWrite
	 *            The metadata value to be written if not already present
	 * @param previouslyAdded
	 *            The values already added (by Tika or before Tika)
	 * @return Whether or not <code>previouslyAdded</code> contains a value similar to <code>valueToWrite</code>
	 */
	protected boolean containsSimilarValue(final T valueToWrite, final Set<T> previouslyAdded) {
		for (final T prev : previouslyAdded) {
			if (this.compareSimilarValues(prev, valueToWrite)) {
				return true;
			}
		}
		return false;
	}


	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "[" + this.property.getName() + "-->" + this.uri + "]";
	}

}
