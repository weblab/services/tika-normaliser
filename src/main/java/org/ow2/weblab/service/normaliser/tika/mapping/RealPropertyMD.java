/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.normaliser.tika.mapping;

import java.util.Collections;
import java.util.Set;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Property.ValueType;
import org.ow2.weblab.service.normaliser.tika.Constants;

/**
 * This subclass of {@link MappingDescription MappingDescription} allows to write as a Double.
 *
 * @author ymombrun
 */
public class RealPropertyMD extends MappingDescription<Double> {


	public RealPropertyMD(final Property property, final String uri, final String prefix, final boolean multivalue, final boolean skipIfExisting) {
		super(property, uri, prefix, multivalue, skipIfExisting, Double.class);
		if (property.getValueType() != ValueType.REAL) {
			final String message = "Property " + property.getName() + " is not Real based. It is a " + property.getValueType() + ".";
			this.logger.fatal(message);
			throw new IllegalArgumentException("'<"+Constants.SERVICE_NAME+">'"+" Error processing the document. "+message);
		}
	}


	@Override
	protected Set<Double> extractPropertyValues(final Metadata metadata) {
		final String dirtyStr = metadata.get(this.property);
		if (dirtyStr == null) {
			return Collections.emptySet();
		}
		final Double value;
		try {
			value = Double.valueOf(dirtyStr);
		} catch (final NumberFormatException nfe) {
			this.logger.warn("Value of property " + this.property.getName() + " is not a valid double.", nfe);
			return Collections.emptySet();
		}

		return Collections.singleton(value);
	}


	@Override
	protected boolean compareSimilarValues(final Double value1, final Double value2) {
		return value1.equals(value2);
	}


}
